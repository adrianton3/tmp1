(() => {
	'use strict'

	// I took the liberty to write 2 small
	// utilities for dealing with the dom

	/**
	 * Generates dom nodes for the supplied template
	 * @param {Array|number|string} template The template in a hiccup-style format
	 * @returns {Node}
	 * @example
	 * generateHtml(
	 *     ['div', { id: 'd1' },
	 *         ['span', {}, 'text']]
	 * )
	 * // generates the node structure <div id="d1"><span>text</span></div>
	 */
	function generateHtml (template) {
		if (!Array.isArray(template)) {
			return document.createTextNode(template)
		}

		const [type, props, ...children] = template

		if (typeof type === 'string') {
			const element = document.createElement(type)

			Object.entries(props).forEach(([key, value]) => {
				element[key] = value
			})

			children.map(generateHtml).forEach((child) => {
				element.appendChild(child)
			})

			return element
		} else if (typeof type === 'function') {
			// this ended up not being used but it is in line with hiccup templates
			return type(props, children)
		} else {
			throw new Error('element type can be a string or a function')
		}
	}


	// this mimics react/reagent's components

	/**
	 * Creates a "data store"/"app". This re-render as soon as its state
	 * has been updated.
	 * @param {{ mountElement: Node, render: Function, initialState: Object }} config
	 * @returns {{ state: Object, setState: Function }}
	 */
	function makeApp ({ mountElement, render, initialState }) {
		const state = Object.assign({}, initialState)
		let stateUpdates = {}

		let rafId
		let root

		function setState (substate) {
			// state updates are accumulated in stateUpdates
			Object.assign(stateUpdates, substate)

			if (!rafId) {
				// updating the dom happens at most once/frame
				rafId = requestAnimationFrame(() => {
					rafId = null

					Object.assign(state, stateUpdates)
					stateUpdates = {}

					const newRoot = generateHtml(render(state))

					if (root) {
						mountElement.replaceChild(newRoot, root)
					} else {
						mountElement.appendChild(newRoot)
					}

					root = newRoot
				})
			}
		}

		setState({})

		return { state, setState }
	}

	window.MyDom = window.MyDom || {}
	Object.assign(window.MyDom, { makeApp })
})()