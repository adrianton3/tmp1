(() => {
	'use strict'

	const { getDistanceAPI } = RawApi

	/**
	 * Wraps the getDistanceAPI call into a promise
	 * @returns {Promise}
	 */
	function getDistance () {
		return new Promise((resolve, reject) => {
			getDistanceAPI(({ distance }) => { resolve(distance) })
		})
	}

	window.NiceApi = window.NiceApi || {}
	Object.assign(window.NiceApi, { getDistance })
})()