(() => {
	'use strict'

	const { makeApp } = MyDom
	const { getDistance } = NiceApi


	const initialState = {
		circle: { name: 'circle', count: 0, mediumArea: 0, latestArea: 0, status: 'idle' },
		square: { name: 'square', count: 0, mediumArea: 0, latestArea: 0, status: 'idle' },
		rectangle: { name: 'rectangle', count: 0, mediumArea: 0, latestArea: 0, status: 'idle' },
		ellipse: { name: 'ellipse', count: 0, mediumArea: 0, latestArea: 0, status: 'idle' },
	}

	function makeUpdater (name, getArea) {
		return () => {
			const state = app.state[name]

			app.setState({
				[name]: Object.assign({}, state, { status: 'pending' })
			})

			getArea().then((area) => {
				const newCount = state.count + 1
				const newMediumArea = (state.mediumArea * state.count + area) / newCount

				const newEntry = {
					count: newCount,
					mediumArea: newMediumArea,
					latestArea: area,
					status: 'idle',
				}

				app.setState({
					[name]: Object.assign({}, state, newEntry)
				})
			})
			// a rejection handler should be present here but our promises/api never fail
		}
	}

	const updaters = {
		'circle': makeUpdater(
			'circle',
			() => getDistance().then((radius) => Math.PI * Math.pow(radius, 2))
		),
		'square': makeUpdater(
			'square',
			() => getDistance().then((size) => Math.pow(size, 2))
		),
		'rectangle': makeUpdater(
			'rectangle',
			() => Promise.all([getDistance(), getDistance()])
				.then(([width, height]) => width * height)
		),
		'ellipse': makeUpdater(
			'ellipse',
			() => Promise.all([getDistance(), getDistance()])
				.then(([radius1, radius2]) => Math.PI * radius1 * radius2)
		),
	}

	function renderShape ({ name, count, mediumArea, latestArea, status }) {
		const onclick = updaters[name]

		return (
			['tr', {},
				['td', {}, name],
				['td', {}, count],
				['td', {}, mediumArea.toFixed(2)],
				['td', {}, latestArea.toFixed(2)],
				['td', {}, ['button', { onclick, disabled: status !== 'idle' }, 're-compute']]]
		)
	}

	function render (shapes) {
		return (
			['table', {},
				['thead', {},
					['tr', {},
						['th', {}, 'shape'],
						['th', {}, 'count'],
						['th', {}, 'medium area'],
						['th', {}, 'latest area'],
						['th', {}, '']]],
				['tbody', {},
					...Object.values(shapes).map(renderShape)]
			]
		)
	}

	const app = makeApp({
		mountElement: document.getElementById('app'),
		render,
		initialState,
	})
})()